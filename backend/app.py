#!/usr/bin/python3
import os
from flask import Flask, render_template, request, redirect
from flask_mail import Mail, Message
from flask_cors import CORS, cross_origin

mail = Mail()

app = Flask(__name__)

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
CORS(app)

app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'dummymaildaw@gmail.com'
app.config['MAIL_PASSWORD'] = 'Ubuntu.2345'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

mail.init_app(app)


	
@app.route('/result', methods = ['POST'])
def result():
    print(request.form['email'])
    if request.form['email'] is None:        
        return "No email information is given" 
    print(request.form['email'])    
    send_message(request.form['email'])
    return redirect("http://localhost:3000/contact", code=302)

def send_message(message):
    print("Catargiu Andreea-Alexandra")
    recipient = message
    msg = Message("Tema 2", sender = app.config.get("MAIL_USERNAME"),
            recipients = [recipient],
            body= "Mesajul acesta este pentru tema 2 la DAW! Cu drag, Catargiu Andreea-Alexandra"
    )  
    mail.send(msg)

if __name__ == "__main__":
    app.run(debug = True)