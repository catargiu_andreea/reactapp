# contact-form-python-flask
Contact form pentru a putea trimite email utilizand Flask

### Trebuie instalate urmatoarele: 

python3.8
pip3 install Flask
pip3 install Flask-WTF
pip3 install Flask-Mail
pip3 install -U flask-cors

### Se ruleaza aplicatia

python3 app.py flask run

## run localhost:5000 (Acesta va fi portul pe care va rula serverul de python)
