import React from 'react'
import {Nav,NavDropdown, Form,Navbar, Button} from 'react-bootstrap';
import './manu.css'
import { useTranslation } from 'react-i18next';


function Manu(){

        const { t, i18n } = useTranslation();
        function handleClick(lang){
            i18n.changeLanguage(lang);
        }

       return(


           <Navbar bg="dark" expand="lg">
               <Navbar.Brand bg="light"><div class="b">
                   <b>Recunoastere faciala</b>
               </div></Navbar.Brand>
               <Navbar.Toggle aria-controls="basic-navbar-nav" />
               <Navbar.Collapse id="basic-navbar-nav">

                   <Nav className="mr-auto" variant="white">
                       <Nav.Link href="acasa"><div class="a">{t('Acasa.1')}</div></Nav.Link>
                       <Nav.Link href="noutati"><div class="a">{t('Noutati.1')}</div></Nav.Link>
                       <Nav.Link href="despreLucrare"><div class="a">{t('DespreLucrare.1')}</div></Nav.Link>
                       <Nav.Link href="profilStudent"><div class="a">{t('ProfilStudent.1')}</div></Nav.Link>
                       <Nav.Link href="coordonator"><div class="a">{t('Coordonator.1')}</div></Nav.Link>
                       <Nav.Link href="contact"><div class="a">{t('Contact.1')}</div></Nav.Link>

                       <Button onClick={()=>handleClick('en')}>
                           English
                       </Button>
                       <Button onClick={()=>handleClick('ro')}>
                           Romana
                       </Button>


                   </Nav>
               </Navbar.Collapse>

           </Navbar>

       );

}
export default Manu
