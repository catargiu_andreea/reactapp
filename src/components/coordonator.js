import React from 'react'
import Manu from './manu.js'
import image from './homebkg.jpg'
import './coordonator.css'
import {Container, Row, Col, Image} from "react-bootstrap";
import copyright from './copyright.png'

var sectionStyle = {
    width: "100%",
    height: "800px",
    backgroundImage: `url(${image})`
};


class Coordonator extends React.Component{
    render() {
        return (
            <div  style={ sectionStyle }>

                <Manu />
                <h1><div class="cooName">Florin Ioan Oniga </div></h1>
                <h3><div class="functie">Functie:  Profesor asociat al departamentului de Calculatoare al Universitatii Tehnice din Cluj-Napoca</div> </h3>
                <h3><div class="projectResearch">Proiecte de cercetare: 1. "Masurarea suprafetelor rutiere folosind stereo", subventie de cercetare finantata de Robert Bosch GmbH </div><div class="projectResearch1">(beneficiar), Germania, 2013</div> </h3>
                <h3><div class="projectResearch2">2. "Identificarea limitelor de benzi 3D bazate pr infrastructura de limitare a drumului si intreruperile de</div><div class="projectResearch22">suprafata prin masurarea stereo", subventie de cercetare finantat de Robert Bosch GmbH (beneficiar),</div><div class="projectResearch222"> Germania, 2014</div>  </h3>
                <h3><div class="projectResearch3">3. "Masurarea conditiilor de suprafata a drumului folosind stereo", subventie internationala de</div><div class="projectResearch33">cercetare finantata de Robert Bosch GmbH (beneficiar), Germania, 2015</div> </h3>

                <h3><div class="copyrightCoordonator">Copyright: Catargiu Andreea-Alexandra</div> </h3>


            </div>


        );

    }
}
export default Coordonator