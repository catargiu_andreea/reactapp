import React from 'react'
import Manu from './manu.js'
import image from './homebkg.jpg'
import './noutati.css'
import {Container, Row, Col, Image, Table} from "react-bootstrap";
import copyright from './copyright.png';



var sectionStyle = {
    width: "100%",
    height: "800px",
    backgroundImage: `url(${image})`
};



function Noutati(){

    var xml2js = require('xml2js');
    var xml = "<noutati>" +
        "<topic>Topic</topic><link>Link</link>" +
        "<titlu1>Recunoastere faciala in stiinta:</titlu1>" +
        "<titlu2>Recunoastere faciala in viata de zi cu zi:</titlu2>" +
        "<topic1>1. De ce recunoastere faciala?</topic1>" +
        "<topic2>2. Recunoasterea faciala folosind jumatate de fata</topic2>" +
        "<topic3>3. Recunoastere faciala pentru autism</topic3>" +
        "<topic4>1. Recunoasterea faciala pentru a urmari oameni</topic4>" +
        "<topic5>2. Recunoasterea faciala a ajutat la salvarea a doi copii</topic5>" +
        "<link1>https://www.gemalto.com/govt/biometrics/facial-recognition</link1>" +
        "<link2>https://www.sciencedaily.com/releases/2019/05/190501114602.htm</link2>" +
        "<link3>https://www.unr.edu/nevada-today/news/2020/emily-hand-research</link3>" +
        "<link4>https://www.nytimes.com/2020/01/18/technology/clearview-privacy-facial-recognition.html</link4>" +
        "<link5>https://globalnews.ca/news/6605675/rcmp-used-clearview-ai-child-exploitation/</link5></noutati>";


    var topic = "";
    var link = "";
    var titlu1 = "";
    var titlu2 = "";
    var topic1 = "";
    var topic2 = "";
    var topic3 = "";
    var topic4 = "";
    var topic5 = "";
    var link1 = "";
    var link2 = "";
    var link3 = "";
    var link4 = "";
    var link5 = "";
    var parser = new xml2js.Parser();
    parser.parseString(xml, function(err,result){

        topic = result['noutati']['topic'];
        link = result['noutati']['link'];
        titlu1 = result['noutati']['titlu1'];
        titlu2 = result['noutati']['titlu2'];
        topic1 = result['noutati']['topic1'];
        topic2 = result['noutati']['topic2'];
        topic3 = result['noutati']['topic3'];
        topic4 = result['noutati']['topic4'];
        topic5 = result['noutati']['topic5'];
        link1 = result['noutati']['link1'];
        link2 = result['noutati']['link2'];
        link3 = result['noutati']['link3'];
        link4 = result['noutati']['link4'];
        link5 = result['noutati']['link5'];
    });

    return (
        <div  style={ sectionStyle }>

            <Manu />



            <Table striped bordered hover>
                <thead>

                <tr>
                    <th>{topic}</th>
                    <th>{link}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{titlu1}</td>

                </tr>
                <tr>
                    <td>{topic1}</td>
                    <td>{link1}</td>

                </tr>
                <tr>
                    <td>{topic2}</td>
                    <td>{link2}</td>

                </tr>
                <tr>
                    <td>{topic3}</td>
                    <td>{link3}</td>

                </tr>
                <tr>
                    <td>{titlu2}</td>


                </tr>
                <tr>
                    <td>{topic4}</td>
                    <td>{link4}</td>

                </tr>
                <tr>
                    <td>{topic5}</td>
                    <td>{link5}</td>

                </tr>


                </tbody>
            </Table>

            <h3><div class="copyrightNoutati">Copyright: Catargiu Andreea-Alexandra</div> </h3>


        </div>

    );


}
export default Noutati