import React from 'react'
import Manu from './manu.js'
import image from './homebkg.jpg'
import {Container, Row, Col, Image, Form, Button} from "react-bootstrap";
import copyright from './copyright.png'
import blue from './try2.png'
import './contact.css'
import msg from './emailContact.jpg'
import {useTranslation} from "react-i18next";

var sectionStyle = {
    width: "100%",
    height: "800px",
    backgroundImage: `url(${blue})`
};


class  Contact extends React.Component{


    constructor(props) {
        super(props);
        this.state = { value: "", email: "" };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        console.log("making request");
        fetch("/result")
            .then((response) => {
                console.log(response);
                return response.json();
            })
            .then((json) => {
                console.log = json;
                this.setState({ email: json[0] });
            });
    }


    render() {
        return (
            <div style={sectionStyle}>

                <Manu/>
                <p>
                    <div class="email">&#8226; Email:</div>
                </p>
                <p>
                    <div class="textEmail">catargiu_andreea.alexandra@yahoo.ro</div>
                </p>
                <p>
                    <div class="tel">&#8226; Telefon:</div>
                </p>
                <p>
                    <div class="telText">0743485420</div>
                </p>
                <p>
                    <div class="adr">&#8226; Adresa:</div>
                </p>
                <p>
                    <div class="adrText">Strada Bucuresti, nr. 68</div>
                </p>
                <p>
                    <div class="adrText2">Alba Iulia, Alba</div>
                </p>
                <p>
                    <div class="msj">Lasa un mesaj</div>
                </p>
                <h3>
                    <div class="copyrightContact">Copyright: Catargiu Andreea-Alexandra</div>
                </h3>

                <Form>
                    <Form.Row>
                        <Col>
                            <div class="form">
                                <Form.Control placeholder="Detalii"/>
                            </div>
                        </Col>
                        <Col>
                            <div class="form1">
                                <Form.Control placeholder="Nume"/>
                            </div>
                        </Col>
                      
                        <Col>
                            <div class="telForm">
                                <Form.Control placeholder="Nr. telefon"/>
                            </div>
                        </Col>
                        <Col>
                            <div class="textForm">
                                <Form.Control placeholder="Introduceti mesajul" as="textarea" rows="3"/>
                            </div>
                        </Col>
                      
                    </Form.Row>
                </Form>

                <div class="textt2">Pentru a primi detalii, puteti introduce doar email-ul dumneavoastra
                                    si apasati submit.</div>

                <div>
                    <div class="newForm"><form
                        onSubmit={this.handleSubmit}
                        action="http://localhost:5000/result"
                        method="POST"
                    >
                         
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Your email"
                            value={this.state.email}
                            onChange={(e) => this.setState({ email: e.target.value })}
                        />
                        <div class="submit"><input
                            type="submit"
                            onClick={(e) => this.handleSubmit(e)}
                            value="Submit"
                        /></div>
                    </form></div>
                </div>
            </div>
        );

    }
}
export default Contact