import React from 'react'
import Manu from './manu.js'
import image from './homebkg.jpg'
import './student.css'
import {Container, Row, Col, Image, Spinner} from "react-bootstrap";
import copyright from './copyright.png'
import pro from './pozaProfil.jpg'

var sectionStyle = {
    width: "100%",
    height: "800px",
    backgroundImage: `url(${image})`
};



class Student extends React.Component{
    render() {
        return (
            <div  style={ sectionStyle }>

                <Manu />

                <h3><div class="copyrightStudent">Copyright: Catargiu Andreea-Alexandra</div> </h3>
                <h3><div class="studentName">Nume: Catargiu Andreea-Alexandra</div> </h3>
                <h3><div class="studentGroup">Grupa: 30641</div> </h3>
                <h3><div class="studentFaculty">Studii: Universitatea Tehnica din Cluj-Napoca, Facultatea de Automatica si Calculatoare, specializarea Tehnologia Informatiei</div> </h3>
                <h3><div class="technology">Tehnologii de interes:</div> </h3>
                <div class="first"><Spinner animation="grow" variant="dark" size="sm"/></div>
                <h3><div class="firstInter">CSS</div> </h3>
                <div class="second"><Spinner animation="grow" variant="dark" size="sm"/></div>
                <h3><div class="secondInter">HTML</div> </h3>
                <div class="third"><Spinner animation="grow" variant="dark" size="sm" /></div>
                <h3><div class="thirdInter">Procesarea imaginilor</div> </h3>
                <div class="fourth"><Spinner animation="grow" variant="dark" size="sm" /></div>
                <h3><div class="fourthInter">Arhitectura Calculatoarelor</div></h3>
                <Container>
                    <Row>
                        <Col xs={6} md={4}>
                            <img src={pro}/>
                        </Col>
                    </Row>
                </Container>


            </div>


        );

    }
}
export default Student