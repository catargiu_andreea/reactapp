import React from 'react'
import Manu from './manu.js'
import image from './homebkg.jpg'
import './despreLucrare.css'
import {Container, Row, Col, Image} from "react-bootstrap";
import copyright from './copyright.png'
import {useTranslation} from "react-i18next";

var sectionStyle = {
    width: "100%",
    height: "800px",
    backgroundImage: `url(${image})`
};


function DespreLucrare(){

        const { t, i18n } = useTranslation();
        function handleClick(lang){
            i18n.changeLanguage(lang);
        }

        return (
            <div  style={ sectionStyle }>
                <Manu />
                <h1><div class="paperName">{t('TitlulLucrarii.1')}</div></h1>
                <h1><div class="paperName1">Recunoastere faciala pentru prezenta</div></h1>
                <h3><div class="description">Obiective principale:</div> </h3>
                <h3><div class="description1"><div class="space">Obiectivul principal al lucrarii de licenta este de a identifica persoane pe baza recunoasterii faciale.
                    O camera de luat vederi va fi plasata la intrarea in incapere (sala de curs/laborator), cu ajutorul careia vom surprinde imagini cu persoanele care
                    vor fi prezente. Dupa ce persoanele surprinse de camera de luat vederi, acestea vor fi identificate cu ajutorul detectiei fetelor, mai apoi
                    fiind trecute ca prezente intr-un tabel. Pentru acest lucru, se va crea un site, unde fiecare profesor si student vor avea cont.
                </div></div> </h3>
                <h3><div class="copyrightLucrare">Copyright: Catargiu Andreea-Alexandra</div> </h3>
            </div>
        );

}
export default DespreLucrare