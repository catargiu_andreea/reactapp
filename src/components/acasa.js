import React from 'react'
        import Manu from './manu.js'
        import image from './homebkg.jpg'
        import './acasa.css'
        import {Container, Row, Col, Image} from "react-bootstrap";
        import copyright from './copyright.png'
import {useTranslation} from "react-i18next";

        var sectionStyle = {
            width: "100%",
            height: "800px",
            backgroundImage: `url(${image})`
        };


        function Acasa() {

            const {t, i18n} = useTranslation();
            return (
                <div style={sectionStyle}>

                    <Manu/>
                    <h1>
                        <div class="c"><b>{t('Titlu.1')}</b></div>
                    </h1>
                    <h3>
                        <div class="bottomRight">{t('Student.1')}</div>
                    </h3>
                    <h3>
                        <div class="bottomRightCoordinator">{t('Coordonator.2')}</div>
                    </h3>
                    <h3>
                        <div class="copyrightAcasa">Copyright: Catargiu Andreea-Alexandra</div>
                    </h3>


                </div>


            );
        }
        export default Acasa