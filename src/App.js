import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Manu from './components/manu'
import Acasa from "./components/acasa"
import Contact from "./components/contact";
import Student from "./components/student";
import Noutati from "./components/noutati";
import DespreLucrare from "./components/despreLucrare";
import Coordonator from "./components/coordonator";
import { BrowserRouter, Route, Switch, Router,Link} from 'react-router-dom';

function App(){

        return(
          <BrowserRouter>
              <div>
                  <Switch>
                      <Route path="/" component={Acasa} exact/>
                      <Route path="/noutati" component={Noutati}/>
                      <Route path="/despreLucrare" component={DespreLucrare}/>
                      <Route path="/contact" component={Contact}/>
                      <Route path="/profilStudent" component={Student}/>
                      <Route path="/coordonator" component={Coordonator}/>
                      <Route path="/acasa" component={Acasa}/>
                      <Route component={Error}/>
                  </Switch>
              </div>
          </BrowserRouter>
        );

}
export default App